#!/bin/sh


Birthrate=$(curl -s http://api.worldbank.org/v2/en/indicator/SP.DYN.CBRT.IN?downloadformat=csv | wc -l)


echo "# TYPE Birthrate gauge"
echo "# HELP birthrate A count of the birthrate"

echo "Birthrate{timeframe=\"year\"} $Birthrate"